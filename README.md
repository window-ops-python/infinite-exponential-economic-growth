# Infinite Exponential Economic Growth

This project explores the concept of infinite economic growth on a finite planet. While it is widely accepted that infinite growth is impossible, this project uses Python and Matplotlib to visualize what such growth would look like.

## Installation

To run this project, you will need to have Python and Matplotlib installed on your computer. You can download Python from the official website, and install Matplotlib using pip:

```
pip install matplotlib
```

## Usage

To run the project, simply run the `plot.py` file using Python:

```
python plot.py
```

This will generate a plot of the infinite exponential economic growth curve.
